import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/style.scss';


export class App extends Component {
	constructor(props) {
		super(props);

	}

	render() {
		return (
			<div>
				<h1>It works!!</h1>
			</div>
		)
	}
}

ReactDOM.render(<App/>, document.getElementById('app'))