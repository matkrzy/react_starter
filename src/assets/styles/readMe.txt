sass/
|
|� base/
|   |� _reset.scss       # Reset/normalize
|   |� _typography.scss  # Regu�y dot. typografii
|   ...                  # Itd�
|
|� components/
|   |� _buttons.scss     # Przyciski
|   |� _carousel.scss    # Karuzela
|   |� _cover.scss       # Ok�adka
|   |� _dropdown.scss    # Rozwijane menu
|   ...                  # Itd�
|
|� layout/
|   |� _navigation.scss  # Nawigacja
|   |� _grid.scss        # Grid system
|   |� _header.scss      # Nag��wek
|   |� _footer.scss      # Stopka
|   |� _sidebar.scss     # Pasek boczny
|   |� _forms.scss       # Formularze
|   ...                  # Itd�
|
|� pages/
|   |� _home.scss        # Style dla strony g��wnej
|   |� _contact.scss     # Style dla konkretnej podstrony
|   ...                  # Itd�
|
|� themes/
|   |� _theme.scss       # G��wny motyw
|   |� _admin.scss       # Motyw dla panelu administratora
|   ...                  # Itd�
|
|� utils/
|   |� _variables.scss   # Zmienne Sassa
|   |� _functions.scss   # Funkcje Sassa
|   |� _mixins.scss      # Mixiny Sassa
|   |� _helpers.scss     # Pomocnicze klasy i selektory
|
|� vendors/
|   |� _bootstrap.scss   # Bootstrap
|   |� _jquery-ui.scss   # jQuery UI
|   ...                  # Itd�
|
|
`� main.scss             # G��wny plik Sassa