var webpack = require('webpack');
var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var SpritesmithPlugin = require('webpack-spritesmith');


var BUILD_DIR = path.resolve(__dirname,'public');
var APP_DIR = path.resolve(__dirname,'src');


var config = {
  context: path.resolve(__dirname,'.'),
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    publicPath: 'http://localhost:8080/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?/,
        include: [
          path.resolve(__dirname,'./src')
        ],
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loaders: ["style","css","sass"],
        exclude: /node_modules/
      },
    ]
  },
  sassLoader: {
    outputStyle: 'compress',
    includePaths:  APP_DIR+[path.resolve(__dirname, "./assets/styles/")]
  },
  resolve: {
    extensions: ["",".js",".jsx"],
    modulesDirectories: ["node_modules"],
    root: APP_DIR
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      removeComments: false,
      collapseWhitespace: false,
      removeRedundantAttributes: false,
      useShortDoctype: false,
      removeEmptyAttributes: false,
      removeStyleLinkTypeAttributes: false,
      keepClosingSlash: true
    }),
    new SpritesmithPlugin({
      src: {
        cwd: path.resolve(__dirname, './src/assets/images/sprites'),
        glob: '*.png,*.svg'
      },
      target: {
        image: path.resolve(__dirname, './src/assets/images/sprite.png'),
        css: path.resolve(__dirname, './src/assets/styles/vendor/sprite.scss')
      },
      apiOptions: {
        cssImageRef: "~sprite.png"
      }
    }),
  ],
  cache: true
};

module.exports = config;